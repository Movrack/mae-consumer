name=mae-consumer
version=1.1.2
release=1
tar_mode=czvf
tar_ext=tar.gz

all: clean tar build

tar :
	tar --exclude-vcs -$(tar_mode) ../$(name)-$(version).$(tar_ext) --transform s/$(name)/$(name)-$(version)/ --exclude=*.rpm --exclude=.settings --exclude=.project --exclude=.pydevproject ../$(name)
	

build: 
	echo "Building $(name) version $(version)-$(release)"
	rpmbuild -ta ../$(name)-$(version).$(tar_ext)
	rpm -i ~/rpmbuild/SRPMS/$(name)-$(version)-$(release).slc6.src.rpm
	rpmbuild -bs ~/rpmbuild/SPECS/$(name).spec
	rpmbuild  --define "_source_filedigest_algorithm 1"  --define "_binary_filedigest_algorithm 1"  --define "_binary_payload w9.gzdio"  --define="dist .slc5"  --define="el5 1"  -bs ~/rpmbuild/SPECS/$(name).spec	
	cp ~/rpmbuild/SRPMS/$(name)-$(version)-$(release).slc5.src.rpm $(name)-$(version)-$(release).slc5.src.rpm
	cp ~/rpmbuild/SRPMS/$(name)-$(version)-$(release).slc6.src.rpm $(name)-$(version)-$(release).slc6.src.rpm

addKoji5:	
	koji add-pkg --owner castor-team castor5 $(name)
	
addKoji6:
	koji add-pkg --owner castor-team castor6 $(name)

buildKoji5:
	koji build castor5 $(name)-$(version)-$(release).slc5.src.rpm 
	
buildKoji6:
	koji build castor6 $(name)-$(version)-$(release).slc6.src.rpm
	
clean :
	find . -name '*.py[co]' -exec rm {} \;
	rm -rf ../$(name)-$(V).tgz $(name)-$(version)-$(release).slc5.src.rpm $(name)-$(version)-$(release).slc6.src.rpm
	

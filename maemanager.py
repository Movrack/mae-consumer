#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import multiprocessing 
import signal
import logging
import pprint
import sys
import time
import ConfigParser
import os
import glob
from optparse import OptionParser, OptionGroup
import shutil
from threading import Timer


from mae import consumer, reducer, utils, rpycserverdeamon
from mae.hadoop.HadoopRequester import HadoopRequester
from mae.hadoop.hadooprequestexception import HadoopInvalidRequestException
from mae.utils import execShellCommand




# Configuration
loggingFormat = "%(asctime)s:%(levelname)-8s %(process)d %(processName)s %(threadName)s :: %(message)s"
#loggingFormatDiff = "%(processName)s %(threadName)s :: %(message)s"
#loggingFormat = loggingFormatDiff
logging.basicConfig(format=loggingFormat,
                    level=logging.INFO)
pprint = pprint.PrettyPrinter(indent=4)

# Global variables
STOP_FLAG = multiprocessing.Event()


def exit_handler(signum=None, frame=None):
    """
    Handler assigned to the reception of SIGTERM and SIGINT signals

    :param signum:
    :param frame:
    """
    logging.info("Main : got stop signal.")
    logging.info("Gently shutting down, could take a while...")
    STOP_FLAG.set()
    

def parse_config(config_file):
    try:
        config = ConfigParser.ConfigParser()
        config.readfp(file(config_file))
    except Exception, e:
        raise RuntimeError('Error while parsing the config:' + str(e))
    
    content = "Config file :\n#####"
    with open(config_file, 'r') as _file:
        for line in _file:
            content += line
    content += "\n#####"
    
    return (config, content)


class Maemanager(object):
    def __init__(self, configFile, options):
        self.name = "Main mae manager"
     
        self.options = options
    
        (self.config, config_content) = parse_config(configFile)
        
        self.log_file_path = self.config.get('mae', "log_file_path")
        
        # Redirect output to log file
        utils.redirect_output(self.log_file_path)
        
        logging.info("logging format: " + loggingFormat)
        logging.info("=============================================================")
        logging.info("==== Starting Metric Analysis Engine ====")
        logging.info("Starting with config file " + configFile)
        logging.info(config_content)
       
                

        self.buffers_size = self.config.getint("Reducer", "buffers_limit")
        self.mae_buffers = []  
        self.maeconsumers = []  
        self.maereducers = [] 
        self.hadoopRequestlock = multiprocessing.Lock()
        self.hadoopRequestList = []
        self.metrics_source_path = self.config.get("mae", "metrics_path")
        self.metrics_source_path_editTime = 0

    def main(self):

        # setup metrics
        self._periodic_metric_folder_check()
        logging.info("timer for checking metrics started")

        #Starting normal streaming mode
        if not self.options.noConsumer : 
            self.runNormalStreamingMode()
            
        # Starting RPyC server    
        logging.debug('starting RPyC server...')
        rpyc_serverDaemon = rpycserverdeamon.RPyCServerDeamon(self.config, STOP_FLAG, self)
        rpyc_serverDaemon.start()

        # Run until exit is asked
        # Here we need to wait "actively", and not wait() on the Event or join(),
        # cause the signals wouldn't be handled otherwise.
        while not STOP_FLAG.is_set():
            time.sleep(1)
            
        # Then exit...
        # consumers
        for maeconsumer_process in self.maeconsumers:
            maeconsumer_process.join()
        #reducers
        for maereducer_process in self.maereducers:
            maereducer_process.join()
        logging.info("EXIT maemanager. Done")

    def _build_metric_folder(self):
        """ 
        Clean working metric folder and set all metrics needed by the analyzer.
        """
        metrics_working_path = self.config.get("mae", "working_dir")+'/metrics'
        metrics = self.config.get("mae", "default_metrics").split()

        logging.info("_build_metric_folder ")
        logging.info("metric_path (_build_metric_folder): " + str(metrics_working_path))
        logging.info("metrics (_build_metric_folder): " + str(metrics))
        
        if os.path.exists(metrics_working_path):
            # clean folder
            utils.clean_folder(metrics_working_path)
            logging.info("path cleaned")
        else :
            # create folder
            os.makedirs(metrics_working_path)
            logging.info("path created")
         
        # select metrics
        if len(metrics) == 1 and metrics[0] in ["all", "All"]:
            logging.info("get all metrics")
            metric_list = os.listdir(self.metrics_source_path) 
        else:
            logging.info("get selected metrics")
            metric_list = [metric.strip() for metric in metrics.split(',')]
        logging.info(str(metric_list))
        # copy metrics
        for metric in metric_list:
            if metric == "unused":
                continue            
            try :
                shutil.copy(self.metrics_source_path + "/" + metric, metrics_working_path + "/" + metric)  
            except IOError :
                logging.error("File " + self.metrics_source_path + "/" + metric + " doesn't exist.")
        # save last edit time.
        logging.info("metrics builded")
        stats = os.stat(self.metrics_source_path)
        self.metrics_source_path_editTime = stats.st_mtime
        
    def _check_and_update_metrics_folder(self):
        """ Check and update metrics if something have changed in metrics_source_path """
        # Access on folder metric ? If yes, probably for changes.
        stats = os.stat(self.metrics_source_path)
        # if same time, do nothing
        if self.metrics_source_path_editTime == stats.st_mtime :
            return
        # redo folder
        self._build_metric_folder()
                
    def _periodic_metric_folder_check(self):
        if STOP_FLAG.is_set(): # break if we want stop
            return
        self._check_and_update_metrics_folder()
        t = Timer(10.0, self._periodic_metric_folder_check)
        t.start()

    def archived_request(self, dateStart, dateEnd, metricNames, instanceName=[], typeName=[], name=None, status_callback=None):
        """ 
        Request on hadoop to fill history or hole in cockpit 
        
        :param metricNames: Names of metrics to process
        :param instanceName: Instance where to find data
        :param typeName: Type where to find data
        :param dateStart: Beginning of range to find
        :param dateEnd: End of range to find
        """
        self._hadoop_request_log(status_callback, 'Checking parameters')
        # verify dates
        if not utils.is_a_date(dateStart):
            raise HadoopInvalidRequestException('dateStart is not a date !')
        
        if not utils.is_a_date(dateEnd):
            raise HadoopInvalidRequestException('dateEnd is not a date !')
       
        if dateStart > dateEnd:
            raise HadoopInvalidRequestException('dateEnd can not precede dateStart !')
        
        # verify metrics
        if 'all' not in metricNames and 'All' not in metricNames and len(metricNames) >= 1:
            available_metrics = os.listdir(self.config.get('mae', 'metrics_path'))
            for metric in metricNames :
                if metric not in available_metrics:
                    raise HadoopInvalidRequestException("Metric '"+metric+"' is not available.")
                        
        # verify path
        for iname in instanceName:
            if "/" in iname:
                raise HadoopInvalidRequestException('instanceName don\'t have a valid name !')
         
        for tname in typeName:
            if "/" in tname:
                raise HadoopInvalidRequestException('typeName don\'t have a valid name !')
        
        self._hadoop_request_log(status_callback, 'Parameter checked')        
        
        workingdir = self.config.get('mae', 'working_dir')+"/"+self.config.get('Hadoop', 'output_dir')
        
        if not os.path.exists(workingdir):
            self._hadoop_request_log(status_callback, "local dir created : " + workingdir)
            os.makedirs(workingdir)  
            
        #hadoopRequester = HadoopRequester(self.config, metricNames, instanceName, typeName, 1, dateStart, dateEnd, name, status_callback, workingdir)        
        #self.hadoopRequestlock.acquire()
        #self.hadoopRequestList.append(hadoopRequester)
        #self.hadoopRequestlock.release()
        
        #hadoopRequester.run()
        #request_full_name = hadoopRequester.dateRequest+"_"+name
        #data_folder_path = hadoopRequester.output_destination+'/'+request_full_name+'/data/part-00000'
        ## Testing
        request_full_name = '2013-08-28T11-00-44.097623_demo'
        data_folder_path = '/var/tmp/consumer-maemanager/hadoop'+'/'+request_full_name+'/data/part-00000'
        
        mae_buffers = []
        mae_buffer = multiprocessing.Queue(self.buffers_size) 
        mae_buffers.append(mae_buffer)
        
        self._hadoop_request_log(status_callback, self.name + ' : starting consumer for '+request_full_name+'...')       
        maeconsumer_process = consumer.Consumer(STOP_FLAG, request_full_name, mae_buffer, self.config, msg_sourceType=('file_reader', 'FileReader'), msg_sourcePath=data_folder_path)
        maeconsumer_process.start()
        
        
        self._hadoop_request_log(status_callback, self.name + ' : starting reducer for '+request_full_name+'...')  
        maereducer_process = reducer.Reducer(STOP_FLAG, request_full_name, mae_buffers, self.config)
        maereducer_process.start()
            
        self.maeconsumers.append(maeconsumer_process)
        self.maereducers.append(maereducer_process) 
            
        #TODO  Deleting result folder ?
       
        while not STOP_FLAG.is_set():            
            self._hadoop_request_log(status_callback, 'Waiting end of analyze.') 
            time.sleep(200) # TODO Fix end of analyse waiting !!! (delete file and check file exist)
            self.maeconsumers.remove(maeconsumer_process)
            self.maereducers.remove(maereducer_process)         
            maeconsumer_process.join()
            maereducer_process.join()
            
            self._hadoop_request_log(status_callback, "Request correctly done")
            return "Request correctly done"
        
        # consumer & reducer will be close by main
        # TODO : closing rpyc connect
        #return "Programme closed by ^C. Request broked."
    
    def _hadoop_request_log(self, callback, message):
        logging.info(message)     
        callback(message)
        
    
    def runNormalStreamingMode(self):                
        """ 
        Default program who start streaming process and thread for the normal cockpit analyze 
        """
        # Consumers
        for processNumber in range(self.config.getint("mae", "number_of_maeconsumer")):
            mae_buffer = multiprocessing.Queue(self.buffers_size) 
            self.mae_buffers.append(mae_buffer)
            
            logging.info(self.name + ' : starting consumer ' + str(processNumber) + ' ...')        
            maeconsumer_process = consumer.Consumer(STOP_FLAG,
                                                    processNumber,
                                                    mae_buffer,
                                                    self.config,
                                                    self.config.get("inputPlugin", "default_source_type").split(" "),
                                                    self.config.get("mae", "working_dir")+"/"+self.config.get("inputPlugin", "source_dir"))
            
            self.maeconsumers.append(maeconsumer_process)
            maeconsumer_process.start()
            
        # Reducers
        maereducer_process = reducer.Reducer(STOP_FLAG, "default", self.mae_buffers, self.config)
        maereducer_process.start()
        self.maereducers.append(maereducer_process)
        
        
        
        
        
    #TODO? refactoring with def runMapReducePipeline(self, nbConsumer, sourceDir, sourceType)

def killAllExistingProcess():
    """ Should be use only for debug! """
    

if __name__ == '__main__':
    # Assign handler to signals
    signal.signal(signal.SIGINT, exit_handler)
    signal.signal(signal.SIGTERM, exit_handler)
    
    # Setting parameters & help
    defaultConfig = "/etc/mae/maemanager.conf"
    usage = "usage: %prog [configurationFile]"
    parser = OptionParser(usage=usage)
    parser.add_option("-c", "--config",
                      dest="configFile", default=defaultConfig,
                      help="configuration file [default : "+defaultConfig+"]")
    parser.add_option('-n', "--noConsumer",
                      action="store_true", dest="noConsumer",
                      help="Don't start consumers of normal streaming mode. Could be used to run for asking with RPyC command.")
    
    debugGroup = OptionGroup(parser, "Dangerous Debug Options",
                    "Caution: use these options at your own risk.  ")
    debugGroup.add_option( "-D", "--debug",
                      action="store_true", dest="debugMode",
                      help="Use debug mode.")
    debugGroup.add_option( "-K", "--killALL",
                      action="store_true", dest="killAll",
                      help="HARD Kill all current process. SHOULD BE USED ONLY FOR DEBUG. It's equivalent to 'ps aux | grep '"+sys.argv[0]+"' | kill -9 '. "
                        "Will not work without debug. In the same time, an \"auto-kill\" will be done.")
    
    parser.add_option_group(debugGroup)
    (options, args) = parser.parse_args()
    if options.configFile:
        configFile = options.configFile
    
    if options.debugMode and options.killAll:
        # USe only for debug !
        killCommand = "ps aux | grep "+sys.argv[0][0:-3]+".p[y] | grep -v grep | awk '{print $2}' | xargs kill -9"

        execShellCommand(killCommand)
        sys.exit()
        
    if options.killAll:
        print "Are you sure you want to kill everything ? If yes, you need to use de debug mode : "+sys.argv[0]+" --debug --killAll"
        sys.exit()
        
    # Start main thread
    maemanager = Maemanager(configFile, options)
    maemanager.main()
    

#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import os
import simplejson as json
import logging

""" 
FileReader input plugin 
Allow to read messages from files
"""
    
class FileReader():  
    def __init__(self, config, params):
        source = params[0]
        self._source = source
             
    def _parse(self, msg):
        """
        Parse the incoming message
        
        :param msg: the message to parse
        :return: None if the message can not be parsed or there is no data in the message. Else the msg parsed
        """
        try : 
            msg = json.loads(msg.strip())
        except:
            self.logging.error("Analyzer: Unable to parse from file the json string :\n"+msg)
            return None
        return msg
                
              
    def streamParsedData(self):   
        """
        Stream data from the source.
        
        """              
        with open(self._source) as f:
            for data in f:
                msg = self._parse(data)
                if msg is not None:
                    logging.debug("file_reader : stream parsed data")
                    yield msg
                    
                    
#>>> cpt = 0
#>>> for f in streamParsedData():
#...     cpt += 1
#...     print cpt
#...     print f
#...     time.sleep(0.1)
#... 
                    
                    
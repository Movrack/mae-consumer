#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

""" 
Example reader. Set it in config file with 

[inputPlugin]
...
default_source_type = plugin_Example ExampleReader
...

"""
    
class ExampleReader():     
    def __init__(self, config, params):
        source = params[0]
        STOP_FLAG = params[1]
        
        self._source = source
        self.STOP_FLAG = STOP_FLAG
           
    
    
    def streamParsedData(self):
        for data in self._source:
            if self.STOP_FLAG.is_set():
                return 
            
            yield self._parseDataInJson(data)
                
    def _parsedataInJson(self, data):
        # do Something on data and parse it in json
        return data
                    
#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************


import sys
import multiprocessing
import threading
import Queue
import threading
import logging
import time
import mae.utils



class Reducer(multiprocessing.Process):
    def __init__(self,  STOP_FLAG, processname, mae_buffers, config):
        multiprocessing.Process.__init__(self, name='Reducer ('+processname+')')
        self.mae_buffers = mae_buffers        
        self.output_buffer =  Queue.Queue(config.getint("Reducer", "buffers_limit"))        
        self.STOP_FLAG = STOP_FLAG
        self.config = config
    
    def run(self):        
        logging.info("started with pid "+str(self.pid))            
             
        reduce_thread = ReduceThread(self.STOP_FLAG, self.mae_buffers, self.output_buffer, self.config)        
        sender_thread = SenderThread(self.STOP_FLAG, self.output_buffer, self.config)
           
        sender_thread.start()  
        reduce_thread.start()      

        while not self.STOP_FLAG.is_set():
            time.sleep(1)        
        reduce_thread.join()
        sender_thread.join()
        logging.info("EXIT reducer")

class PluginWorker(threading.Thread):
  '''Worker thread, responsible for sending data to the output plugins'''

  def __init__(self, pluginChain, workqueue):
    '''constructor'''
    super(PluginWorker, self).__init__(name='PluginWorker')
    # the chain of plugins
    self.pluginChain = pluginChain
    # the queue to work with
    self.workqueue = workqueue
    # whether to continue running
    self.running = True
    # start the thread
    self.setDaemon(True)
    self.start()

  def stop(self):
    '''Stops the thread processing'''
    self.running = False

  def run(self):
    '''main method to the threads. Gets data from the queue and call plugins.
       In case the queue contains more data, group data in bunches'''
    while self.running:
      try:
        newdata = self.workqueue.get(True, 0.5)
        data = newdata
        c = 0
        while newdata and c < 100:
          try:
            newdata = self.workqueue.get(False)
            data.update(newdata)
            c += 1
          except Queue.Empty:
            break
        for plugin in self.pluginChain:
          data = plugin.apply(data) 
      except Queue.Empty:
        # we've timed out, let's just retry. We only use the timeout so that this
        # thread can stop even if there is nothing in the queue
        pass
      except Exception, e:
        # "Caught exception in PluginWorker thread" message
        logging.error("Caught exception in PluginWorker thread : type=" + str(e.__class__) + ", Message=" + str(e))
        
class SenderThread(threading.Thread):
    def __init__(self, STOP_FLAG, output_buffer, config):
        # initilization
        threading.Thread.__init__(self, name='senderThread')
        self.STOP_FLAG = STOP_FLAG
        self.config = config
        self.output_buffer = output_buffer
        self.output_chain = []
        # instantiation of plugins
        self.output_plugin_path = config.get("Reducer", "plugin_path")
        self.output_plugin_list = [ tuple(line.split(' ')) \
                        for line in config.get("Reducer", "output_plugins").split('\n') \
                        if line ]
        self.instantiate_output_plugins(self.output_plugin_list)
        # a queue of data to be sent to the plugins
        self.pluginDataQueue = Queue.Queue()
        # a thread pool of PluginWorkers
        self.workers = []
        nbWorkers = int(config.get("Reducer", "nb_plugin_workers", 10))
        for i_unused in range(nbWorkers):
            t = PluginWorker(self.output_chain, self.pluginDataQueue)
            self.workers.append(t)
        logging.info('plugin worker thread pool created (%d threads)' % nbWorkers)
        logging.info("Started ...") 
        
    def instantiate_output_plugins(self, plugins_list):  
        if not self.output_plugin_path in sys.path:
            sys.path.append(self.output_plugin_path)
        try:
            self.output_chain = [ mae.utils.create_plugin(plugin[0], plugin[1], self.config) for plugin in plugins_list ]
        except mae.utils.ConfigError, exc:
            logging.critical(str(exc))
        logging.info('Following plugins are loaded : ' + str(self.output_chain))

    def run(self):      
        while not self.STOP_FLAG.is_set():
            try :
                data = self.output_buffer.get(timeout=1)
            except Queue.Empty:
                # New loop to check STOP_FLAG
                logging.debug("Empty queue. Continuing")
                continue
            logging.debug("sending data to output plugins : " + str(data) + ", queue is " + str(self.pluginDataQueue.qsize()) + " items long")
            self.pluginDataQueue.put(data)
        
        logging.info("exiting pool in sender thread") 
        # waiting some late data.
        time.sleep(1)
        try :
            data = self.output_buffer.get(timeout=1)            
            self.pluginDataQueue.put(data)
        except Queue.Empty:
            pass 
        # stop all workers
        for w in self.workers:
            w.stop()
        # join the worker threads
        for w in self.workers:
            w.join()
        logging.info("EXIT sender thread") 

            
class ReduceThread(threading.Thread):
    def __init__(self, STOP_FLAG, mae_buffers, output_buffer, config):
        threading.Thread.__init__(self, name='reducerThread')
        self.STOP_FLAG = STOP_FLAG
        self.mae_buffers = mae_buffers
        self.output_buffer = output_buffer
        self.reducedData = {}
        logging.info("Started ...") 
        self.live_wait_time = config.getint("Reducer", "live_wait_time")
        self.backlog_wait_time = config.getint("Reducer", "backlog_wait_time")
        self.nbThread = config.getint("mae", "number_of_maeconsumer")
        
        
    def run(self):      
        while not self.STOP_FLAG.is_set(): 
            for data in self._getNewDataToReduce():
                if data is not None:
                    self._reduce(data)
                self._flush_data()
            self._flush_data() # if data == None
            time.sleep(1)
                
        logging.info("EXIT reduceThread")
    
    def _is_backlog(self, used_timestamp):
        currentTime = time.time()
        return (used_timestamp + self.live_wait_time) < currentTime
    
        
    def _flush_data(self):
        data_to_push = {}
        for metric in self.reducedData:
            timestamps_for_metric = self.reducedData[metric].keys()
            
            for timestamp in timestamps_for_metric:
                
                # No data ? Go next
                if self.reducedData[metric][timestamp]['bin'] is None:
                    continue
                
                data_to_push = {}
                currentTime = time.time()
                mode = self.reducedData[metric][timestamp]['mode']
                timeToWait = self.backlog_wait_time if mode == 'backLog' else  self.live_wait_time

                
                # check if new msg & initialise timer
                if not 'startTime' in self.reducedData[metric][timestamp] :
                    currentTime = time.time()
                    logging.debug (mode+" new data for metric "+str(metric)+" at "+str(timestamp)+". Will be sent at "+str(currentTime+timeToWait))
                    self.reducedData[metric][timestamp]['startTime'] = currentTime                   
                
                # if not, check allowed to send
                elif (self.reducedData[metric][timestamp]['startTime'] + timeToWait) >= currentTime:
                    
                    logging.debug (mode+" data to send for metric "+str(metric)+" at "+str(timestamp))
                    _bin = self.reducedData[metric][timestamp]['bin']
                    _bin.compute_data()
                    data_to_push[metric] = {
                        'timestamp' : timestamp, 
                        'metricData' : _bin.getData()
                    }
                    del(self.reducedData[metric][timestamp])
                    
                    logging.debug("DATA TO PUSH from "+mode+" at "+str(time.time())+' '+ str(data_to_push))
                    self.output_buffer.put(data_to_push)
                    data_to_push = {}
                
        ## clean dict to avoid memory leak
        # del(self.reducedData[metric]) # not allowed when iterating on it self. So we need to find first which to remove
        
        metricToRemove = []
        for metric in self.reducedData :
            if 0 == len(self.reducedData[metric]):
                metricToRemove.append(metric)
        for metric in metricToRemove:
            del(self.reducedData[metric]) # created in _reduce(data)
                    
        
            
    def _reduce(self, data):
        for metric in data:
            
            # metric goes for first time in reducer
            if not metric in self.reducedData :
                self.reducedData[metric] = {} # cleaned in _flush_data()
            
            # No data, nothing to do
            if data[metric] is None :
                continue
            
                
            # new timestamp -> add
            if not data[metric]['timestamp'] in self.reducedData[metric] :  
                mode = 'backLog' if self._is_backlog(data[metric]['timestamp']) else 'live'
                self.reducedData[metric][data[metric]['timestamp']] = { 
                    'bin' : data[metric]['metricData'], 
                    'mode' : mode
                }
            
            # existing timestamp -> merge
            else :  
                self.reducedData[metric][data[metric]['timestamp']]['bin'].mergeWith(data[metric]['metricData'])
                
             
    
    def _getNewDataToReduce(self):     
        data_to_reduce = None
        
        logging.info("read")
        while not self.STOP_FLAG.is_set():
            for mae_buffer in self.mae_buffers:
                    
                logging.debug("qsize = "+str(mae_buffer.qsize()))
                try :
                    data_to_reduce = mae_buffer.get(False, timeout=1)
                    logging.info('data to reduce : ' + str(data_to_reduce))
                except Queue.Empty:
                    # New loop to check STOP_FLAG
                    if not self.STOP_FLAG.is_set(): 
                        continue
                except IOError:
                    logging.info("Interupt system call")
                    break
                
                yield data_to_reduce
            time.sleep(0.1)
                
    

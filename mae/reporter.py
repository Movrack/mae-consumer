#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

# Imports

import threading
import logging
import time
import traceback
import os

from mae.metric import Metrics

#class Reporter(multiprocessing.Process):
class Reporter(threading.Thread):
    """
    This thread is in charge of:
    1) Checking for new/removed/modified metrics
    2) Sending new data to the shared buffer

    :param analyzer_process: reference to the analyzer thread (here we update its metric list)
    :param metrics_path: path to the metrics directory
    :param cockpit_enabled:
    :param cockpit_buffer_lock:
    :param cockpit_buffer:
    """

    def __init__(self, analyzer_thread, metrics_path, 
                 reducer_buffer, STOP_FLAG, processNumber="0"):
        threading.Thread.__init__(self, name='Reporter '+str(processNumber))
        self.processNumber = processNumber
        self.Analyzer = analyzer_thread
        self.STOP_FLAG = STOP_FLAG
        self._metrics_path = metrics_path
        self._reducer_buffer = reducer_buffer
        self._reducer_last_sent = dict() # last sent timestamp, to avoid pushing multiple times the same data
        self.metrics_source_path_editTime = 0
        
    def run(self):
        """
        Run the thread...
        Main loop :
            1) updates running metrics in analyzer thread
            2) push new data to cockpit buffer
        """
        logging.info(self.name+" : started")
        self._periodic_update_analyzer_metric()
                    
        while not self.STOP_FLAG.is_set():
            time.sleep(0.1)
            self._send_data_to_reducer()
        logging.info("EXIT reporter")

    def _send_data_to_reducer(self):
        """
        Send the new data to the cockpit buffer
        """
        self._clean_last_sent()
        logging.debug("size before fetch " + str(self._reducer_buffer.qsize()))
        data_to_push = self._fetch_data_for_metrics()
        self._push(data_to_push)
        logging.debug("size after push " + str(self._reducer_buffer.qsize()))
        
        
    def _push(self, data_to_push): 
        # push data only if there is data to be pushed or the buffer is not empty
        if [ data_to_push[name] for name in data_to_push.keys() if data_to_push[name] ] :
            #logging.info("push : " +name + ' - ' + str(data_to_push[name]))
            # here we are sure there is data to be pushed 
            # put data to be sent in the buffer
            self._reducer_buffer.put(data_to_push)
            
            
                
                           
    def _fetch_data_for_metrics(self):        
        push_data = dict()
        
        # fetch data for all running metrics
        for metric in self.Analyzer.metrics:
            #firstly skip lemon and test metrics
            if '-lemon' in metric.name or '-test' in metric.name:
                continue
            # skip not ready metrics
            if not metric.metricBins.ready and metric.name in push_data:
                del push_data[metric.name]
                continue
            
            try:                            
                metric_data = metric.getData("Queue", timeout=0.01)
                
            except Exception:
                logging.exception("Error in processing metric "+metric.name+":\n"+traceback.format_exc())
                # Skip the rest...
                continue
            else:
                if metric_data == None :
                    continue
                
                # skip already sent data
                if metric in self._reducer_last_sent.keys() \
                and metric.metricBins.binValidFrom == self._reducer_last_sent[metric] \
                and metric.name in push_data:
                    del push_data[metric.name]
                    continue

                # put data in dict
                push_data[metric.name] = dict()
                push_data[metric.name]['metricData'] = metric_data
                push_data[metric.name]['timestamp'] = metric.metricBins.binValidFrom
                # update the last time the metric was updated
                self._reducer_last_sent[metric] = metric.metricBins.binValidFrom

        if push_data:
            logging.info("data to push : "+str(push_data))
        else:
            logging.debug("no new data to push")
            time.sleep(0.2)
        return push_data
                
                
    def _periodic_update_analyzer_metric(self):
        """
        Function used to cross check for added or removed metrics
        """
        
        if self.STOP_FLAG.is_set(): # break if we want stop
            return
        
        t = threading.Timer(10.0, self._periodic_update_analyzer_metric)
        t.start()
            
        # Access on folder metric ? If yes, probably for changes.
        stats = os.stat(self._metrics_path)
        # if same time, do nothing
        if self.metrics_source_path_editTime == stats.st_mtime :            
            return
        self.metrics_source_path_editTime = stats.st_mtime
        
        # Reload the metrics:
        new_metrics = Metrics.loadMetrics(self._metrics_path + '/*.metric')

        self._find_added_metrics(new_metrics)
        self._find_updated_metrics(new_metrics)
        
        
    def _find_updated_metrics(self, new_metrics):
        # Find removed  and updated metrics:
        for metric in self.Analyzer.metrics:

            # We now look if metric.name is still in the list (of the new_metrics)
            found = False
            for new_metric in new_metrics:
                if new_metric.name == metric.name:
                    # We still have the metric loaded, set the flag
                    found = True
                    # But now, check if something in the metric changed:
                    changed = False
                    if metric.window != new_metric.window: changed = True
                    if metric.nbins != new_metric.nbins: changed = True
                    if metric.conditions != new_metric.conditions: changed = True
                    if metric.groupbykeys != new_metric.groupbykeys: changed = True
                    if metric.datakeys != new_metric.datakeys: changed = True
                    if metric.dataobjects != new_metric.dataobjects: changed = True
                    if metric.handle_unordered != new_metric.handle_unordered: changed = True
                           
                    # if something changed, reload the metric:
                    if changed:
                        logging.info("Reloading metric " + metric.name)
                        self.Analyzer.metrics.remove(metric)
                        self.Analyzer.metrics.append(new_metric)

            if not found:    
                # The metric has been removed, remove it from the list:
                logging.info("Removing metric " + metric.name)
                self.Analyzer.metrics.remove(metric)

    def _find_added_metrics(self, new_metrics):
    
        # Find added metrics:
        for new_metric in new_metrics:
         
            # We now look if metric.name is still in the list (of the new_metrics)
            found = False
            for metric in self.Analyzer.metrics:
                if metric.name == new_metric.name:
                    # The metric is already loaded
                    found = True
                
            if not found:    
                # The metric has been removed, append it to the list:
                logging.info("Adding metric " + new_metric.name)
                self.Analyzer.metrics.append(new_metric)       
                
    def _clean_last_sent(self):
        """ Clean the last_sent dict by removing the unused metrics """
        for m in self._reducer_last_sent.keys():
            if m not in self.Analyzer.metrics:
                del self._reducer_last_sent[m]

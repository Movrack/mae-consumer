#!/usr/bin/python
# -*- coding: utf-8 -*-
#******************************************************************************
#                     Metric Analysis Engine
#
# Copyright (C) 2011  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

##############################################################################
# Note: metrics will be loaded from the Metricspath folder and the computed. #
# If a metric is malformed it just won't be loaded. If the metric raises an  #
# error at runtime, it will be ignored and the error printed, the normal     #
# operations will continue.                                                  #
##############################################################################

"""
AI consumer for the Metric Analysis Engine
"""

# Imports
import multiprocessing
import logging
import analyzer
import reporter
import time

class Consumer(multiprocessing.Process):
    def __init__(self, STOP_FLAG, processNumber, reducer_buffer, config, msg_sourceType=None, msg_sourcePath=None):
        
        multiprocessing.Process.__init__(self, name='Consumer ' + str(processNumber))
        self.config = config
        self.STOP_FLAG = STOP_FLAG
        self.processNumber = processNumber
        self.message_source_type = msg_sourceType
        self.message_source_path = msg_sourcePath
        self.isDone = False
        self.reducer_buffer = reducer_buffer        
        
    def run(self):
        logging.info("started with pid " + str(self.pid))
        self.start_analyzer()
        logging.info("analyzer started")
        self.start_reporter()
        logging.info("reporter started")
        
        # Exit        
        while not self.STOP_FLAG.is_set():
            time.sleep(1)
        self.analyzer_thread.join()
        self.reporter_thread.join()
        
        self.isDone = True
        logging.info("EXIT consumer")
        
    def start_analyzer(self):
        logging.info('starting analyzer...')
        self.analyzer_thread = analyzer.Analyzer(self.config.get("mae", "working_dir")+'/metrics',
                                                 self.config.get("mae", "working_dir")+'/dirqueue',
                                                 self.STOP_FLAG,
                                                 self.config,
                                                 processNumber=self.processNumber,
                                                 pluginType=self.message_source_type)
        self.analyzer_thread.start()
    
    def start_reporter(self):    
        logging.info('starting reporter...')
        self.reporter_thread = reporter.Reporter(self.analyzer_thread,
                                                 self.config.get("mae", "working_dir")+'/metrics',
                                                 self.reducer_buffer,
                                                 self.STOP_FLAG,
                                                 processNumber=self.processNumber)
        self.reporter_thread.start()

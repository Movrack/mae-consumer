#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import threading
import logging
import commands
import time 
import os

import rpyc
from rpyc.utils.server import ThreadedServer

from mae.threadpoolserver import ThreadPoolServer
from mae.metric import Metrics

from mae.hadoop.hadooprequestexception import HadoopInvalidRequestException

    
class RPyCServerDeamon(threading.Thread):
    """
    Simple thread that starts the RPyC server independently, because t.start() is blocking
    """

    def __init__(self, config, STOP_FLAG, maemanager):
        threading.Thread.__init__(self, name="RPyCServer")
        RPyCServerDeamon.maemanager = maemanager
        RPyCServerDeamon.metrics_path = config.get("mae", "metrics_path")              
        self.STOP_FLAG = STOP_FLAG

        RPyCServerDeamon.metrics = Metrics.loadMetrics(RPyCServerDeamon.metrics_path+'/*.metric')
        logging.info("Metrics loaded for rpyc server :\n "+str(RPyCServerDeamon.metrics))
    
        RPyCServerDeamon.metrics_source_path_editTime = 0
        RPyCServerDeamon.port = config.getint("mae", "rpyc_port")
        self.setDaemon(True)
    
    def __del__(self):
        logging.info("waiting 16 sec to wait all threads before exit (metric updater timer)")
        time.sleep(16)
        
    def metricUpdater(self):
        if self.STOP_FLAG.is_set():
            return
        
        timer = threading.Timer(15, self.metricUpdater)
        timer.start() 
        
        # Access on folder metric ? If yes, probably for changes.
        stats = os.stat(RPyCServerDeamon.metrics_path)
        # if same time, do nothing
        if RPyCServerDeamon.metrics_source_path_editTime == stats.st_mtime :
            return
        
        RPyCServerDeamon.metrics_source_path_editTime  = stats.st_mtime
        RPyCServerDeamon.metrics = Metrics.loadMetrics(RPyCServerDeamon.metrics_path+'/*.metric')
        logging.debug("Metrics reloaded for rpyc server = "+str(RPyCServerDeamon.metrics))
    
    
    def run(self):
        logging.info("RPyC Server : started")
        
        self.metricUpdater()        
        logging.info("Metric list cron updater : started")
        
        # Spawn a thread for each connection
        self.service = ThreadedServer(RPyCService, 
                       port=RPyCServerDeamon.port , 
                       protocol_config={"allow_public_attrs":True}, 
                       auto_register=False)
        
        #self.service = ThreadPoolServer(RPyCService,
        #                 port = RPyCServerDeamon.port,
        #                 auto_register = False,
        #                 protocol_config = {"allow_public_attrs":True},
        #                 nbThreads = 20
        #                 )

        
        self.service.start()


class RPyCService(rpyc.Service):
    """
    RPyC Service providing data to the outside world

    @todo : improve security (dos attack)
    @todo : caching
    @todo : exception in get_metric() if metric not found
    """

    Analyzer = None

    
    #-----------------------------------------------------------------
    def __init__(self, conn):
        """
        Init...
        """
        threading.current_thread().name = "RPycService"+threading.current_thread().name
        rpyc.Service.__init__(self, conn)     
        
        logging.debug("rpycservice connection requested")
    
    def exposed_get_metric(self, name=None):
        """
        If name is provided, return the metric with name == name
        Else, return a tuple of all the running metrics
        :param name: name of the metric
        """

        if name:
            for metric in RPyCServerDeamon.metrics:
                if metric.name == name :
                    logging.info("request for "+name)
                    return metric
            logging.warning("request for "+name+" but not found")
            return None
        logging.info("request for all metric")

        return tuple(RPyCServerDeamon.metrics)
    
    def exposed_get_metric_file(self, name):
        """
        cat the metric file and send the data
        :param name: name of the metric you want
        """
        for metric in RPyCServerDeamon.metrics:
            if metric.name != name :
                continue
            logging.info("request for file "+name)
            return commands.getoutput("cat "+RPyCServerDeamon.metrics_path+"/"+name+".metric")
        logging.warning("request for file "+name+" but not found")
        
        
    def exposed_archived_requestO(self, dateStart, dateEnd, metricNames=[],  instanceName=[], typeName=[], name=None, printer=None): 
        try :
            logging.info("Hadoop archived requested")
            res = RPyCServerDeamon.maemanager.archived_request(dateStart, dateEnd, metricNames, instanceName, typeName, name, printer)
            logging.info('Hadoop request success')
            return res
        
        except HadoopInvalidRequestException, e:
            logging.info("Request refused: "+str(e.message))
            return e.message
    
    class exposed_hadoop_request(object):  
        def __init__(self, dateStart, dateEnd, metricNames=[],  instanceName=[], typeName=[], name=None, status_callback=None):
            self.dateStart = dateStart
            self.dateEnd = dateEnd
            self.metricNames = metricNames
            self.instanceName = instanceName
            self.typeName = typeName
            self.name = name
            
            self.maemanager = RPyCServerDeamon.maemanager
            self.status_callback = rpyc.async(status_callback)
            self.thread = threading.Thread(target = self.work)
            self.thread.start()
            
        def exposed_stop(self):   # this method has to be exposed too
            self.active = False
            self.thread.join()
        
        def work(self):
            time.sleep(1)
            self.status_callback("Request received")
            logging.info("Hadoop archived requested")
            
            try :
                res = self.maemanager.archived_request(self.dateStart, self.dateEnd, self.metricNames, self.instanceName, 
                                                       self.typeName, self.name, self.status_callback)            
        
            except HadoopInvalidRequestException, e:
                msg = "Request refused: "+str(e.message)
                self.status_callback(msg)
                logging.info(msg)
                return
            
            logging.info('Hadoop request success')
            self.status_callback("Request success")
            
            return res
        
              

    '''
    #-----------------------------------------------------------------
    def exposed_get_all_data(self):
        """
        Send data for all running metrics
        """
        data = {}

        for metric in RPyCServerDeamon.metrics:

            if not metric.metricBins.ready: 
                continue        

            try:                               
                metricData=metric.getData("AllShiftedByOne")
            except Exception:
                logging.exception("RPyC : Error in processing metric "+metric.name+":\n"+traceback.format_exc())
                # Skip the rest...
                continue
            else:
                data[metric]={}
                data[metric]['metricData']=metricData
                data[metric]['timestamp']=metric.metricBins.binValidFrom
        
        return data

    #-----------------------------------------------------------------
    def exposed_get_data(self, metric):
        """
        Send data for the specified metric
        :param metric: 
        """
        data = {}

        if metric in self.Analyzer.metrics:
            
            if not metric.metricBins.ready: 
                return data        

            try:                               
                metricData=metric.getData("AllShiftedByOne")
            except Exception:
                logging.exception("RPyC : Error in processing metric "+metric.name+":\n"+traceback.format_exc())
                # Skip the rest...
                return data
            else:
                data[metric] = {}
                data[metric]['metricData'] = metricData
                data[metric]['timestamp'] = metric.metricBins.binValidFrom
        return data

    #-----------------------------------------------------------------
    def exposed_get_json_data(self, metric):
        """
        Send data for the specified metric
        :param metric: 
        """
        data = {}

        if metric in self.Analyzer.metrics:
            
            if not metric.metricBins.ready: 
                return json.dumps(data)

            try:                               
                metricData=metric.getData("AllShiftedByOne")
            except Exception:
                logging.exception("RPyC : Error in processing metric "+metric.name+":\n"+traceback.format_exc())
                # Skip the rest...
                return json.dumps(data)
            else:
                data[metric] = {}
                data[metric]['metricData'] = metricData
                data[metric]['timestamp'] = metric.metricBins.binValidFrom
                # now that we have the data, we return it
                return json.dumps(data[metric])
        return json.dumps(data)

    #-----------------------------------------------------------------
    def exposed_get_data_by_name(self, name):
        """
        Send data of the metric with the given name
        :param name: name of the metric you want
        """
        
        data = {}

        for metric in self.Analyzer.metrics:

            if metric.name != name :
                continue

            if not metric.metricBins.ready: 
                return data

            try:                               
                metricData=metric.getData("AllShiftedByOne")
            except Exception:
                logging.exception("RPyC : Error in processing metric "+metric.name+":\n"+traceback.format_exc())
                # Skip the rest...
                continue
            else:
                data[metric] = {}
                data[metric]['metricData'] = metricData
                data[metric]['timestamp'] = metric.metricBins.binValidFrom
                # we can return now, cause we have what we want
                break
        return data
'''
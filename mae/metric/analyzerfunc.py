#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import math
#######################################################################
#                                                                     #
#                              Data Objects                           #
#                                                                     #
#######################################################################


#######################################################################
#   Class Top
#######################################################################
class Top(dict):
    """
    Return the top x param by number of appaerance
    """
    def __init__(self, init_dict=None):
        if init_dict == None:
            self.data = dict()
        else:
            self.data = init_dict

    def __add__(self, other):
        result = dict()
        # copy self
        for element in self.data:
            result[element] = int(self.data[element])
        # insert element of the other
        for element in other.data:
            if element in result:
                result[element] += other.data[element]
            else:
                result[element] = other.data[element]
        return self.__class__(result) 

    def add(self, element):
        if element in self.data:
            self.data[element] += 1
        else:
            self.data[element] = 1

    def getData(self):
        return self.data


#######################################################################
#   Class Top5
#######################################################################
class Top5(Top):
    """
    Return the top 5 param by number of appaerance
    """
    def getData(self):
        top5_elements = sorted(self.data, key=self.data.get, reverse=True)
        result = dict()
        for element in top5_elements[:5]:
            result[element] = self.data[element]
        return result


#######################################################################
#   Class Top10
#######################################################################
class Top10(Top):
    """
    Return the top 10 param by number of appaerance
    """
    def getData(self):
        top5_elements = sorted(self.data, key=self.data.get, reverse=True)
        result = dict()
        for element in top5_elements[:10]:
            result[element] = self.data[element]
        return result


#######################################################################
#   Class Avg
#######################################################################

class Avg(list):
    """
    A data object that provide Averages
    """

    # def __init__(self, start=None):
    #    if (start==None):
    #        self.extend([None,None,None,None,None])
    #    else:
    #        self=start

    def __add__(self, other):

        # Merge two Avg objects:

        # Check if one of the two is empty. Here instead of returning self or other,
        # we should create a new element, but with numeric types it is fine also like that.
        if (len(self) == 0):
            return other
        if (len(other) == 0):
            return self

        # n:
        n = self[0] + other[0]
        
        # Min:
        if (other[1] < self[1]):
            Min = other[1]
        else:
            Min = self[1]

        # Max:
        if (other[2] > self[2]):
            Max = other[2]
        else:
            Max = self[2]

        # Sum:
        Sum = self[3] + other[3]

        # Sum_sqr        
        Sum_sqr = self[4] + other[4]

        # We return a *new* object with the new values
        return Avg([n, Min, Max, Sum, Sum_sqr])


    def add(self, element):

        # Add an element

        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            try:
                element = float(element)
            except ValueError:
                # If we are unable to conver it, skip.
                return None
                 
        
        # If this is the first element to be added to the object, initialize it
        if (len(self) == 0):
            self.extend([None, None, None, None, None])
            self[0] = 1  # n
            self[1] = element  # Min
            self[2] = element  # Max
            self[3] = element  # Sum
            self[4] = element * element  # Sum_sqr

        else:
            # n:
            self[0] += 1
            
            # Min:
            if (element < self[1]):
                self[1] = element

            # Max:
            if (element > self[2]):
                self[2] = element

            # Sum:
            self[3] += element

            # Sum_sqr:        
            self[4] += (element * element)

        return None


    def getData(self):
        if len(self) == 0:
            return None
        else:
        
            # http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
            # mean = Sum/n
            # variance = (Sum_sqr - Sum*mean)/(n - 1)
            
            n = self[0]
            Sum = self[3]
            Sum_sqr = self[4]
            Min = self[1]
            Max = self[2]
            mean = Sum / n
            
            if (n != 1):
                variance = (Sum_sqr - Sum * mean) / (n - 1)
            else:
                variance = 0.0

            stddev = math.sqrt(variance)

            # return [n, Sum, mean, Min, Max, stddev]
            # return [mean, n, Sum , Min, Max, stddev]
            return mean


#######################################################################
#   Class StdDev
#######################################################################

class StdDev(Avg):
    def getData(self):
        if len(self) == 0 or self[0] < 2:
            return None
        else:
        
            # http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
            # mean = Sum/n
            # variance = (Sum_sqr - Sum*mean)/(n - 1)
            
            n = self[0]
            Sum = self[3]
            Sum_sqr = self[4]
            Min = self[1]
            Max = self[2]
            mean = Sum / n
            
            variance = (Sum_sqr - Sum * mean) / (n - 1)

            stddev = math.sqrt(variance)

            # return [n, Sum, mean, Min, Max, stddev]
            # return [mean, n, Sum , Min, Max, stddev]
            return stddev


#######################################################################
#   Class EstimateDelay
#######################################################################

class EstimateDelay(list):
    """
    To estimate unordered messages mean delay
    """

    def __add__(self, other):

        # Merge two Avg objects:
        if (len(self) == 0):
            return other
        if (len(other) == 0):
            return self

        # nb:
        anb = self[0] + other[0]        
        # min:
        if (other[1] < self[1]):
            amin = other[1]
        else:
            amin = self[1]
        # max:
        if (other[2] > self[2]):
            amax = other[2]
        else:
            amax = self[2]
        # sum:
        asum = self[3] + other[3]

        # sqsum:        
        asqsum = self[4] + other[4]
        
        return EstimateDelay([anb, amin, amax, asum, asqsum])


    def add(self, element):

        # Add an element

        if element == '':
            return None 

        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
            
        if element == 0:
            return None 
        
        # If this is the first element to be added to the object, initialize it
        if (len(self) == 0):
            self.extend([None, None, None, None, None])
            self[0] = 1  # nb
            self[1] = element  # min
            self[2] = element  # max
            self[3] = element  # sum
            self[4] = element * element  # sqsum

        else:
            # nb:
            self[0] += 1
            
            # min:
            if (element < self[1]):
                self[1] = element

            # max:
            if (element > self[2]):
                self[2] = element

            # sum:
            self[3] += element

            # sqsum:        
            self[4] += (element * element)

        return None

    def getData(self):
    
        if len(self) == 0:
            return[0, 0, 0, 0, 0]
        else: 
            #         nb           mean        min     max   sq.sum
            return [self[0], self[3] / self[0], self[1], self[2], self[4]]



#######################################################################
#   Class DummyStore
#######################################################################
class DummyStore(list):
    """
    To store entries as they are found (for data mining).
    """

    def __init__(self, initlist=None):
        if initlist == None:
            self.data = []
        else:
            self.data = initlist

    def __add__(self, other):
    
        # Merge two lists
        return DummyStore(self.data + other.data) 

    def add(self, element):

        # Add an element
        self.data.append(element)

    def getData(self):
        return self.data
      
        

#######################################################################
#   Class ListOne
#######################################################################
class ListLast(list): 
    """
    To store one entry (the last one found), to have an idea of what is in a field (for data mining).
    """
       
    def __init__(self, initlist=None):
        if initlist == None:
            self.data = []
        else:
            self.data = initlist

    def __add__(self, other):

        # It keeps at every step the last element found
        return ListLast(other.data) 

    def add(self, other):

        # It keeps at every step the last element found
        self.data = [other]       

    def getData(self):
        return self.data



#######################################################################
#   Class ListUnique
#######################################################################
class ListUnique(list):
    """
    To store uniques entries found i.e.: 16MB, 32MB, 16MB -> 16MB, 32MB
    """

    def __init__(self, initlist=None):
        if initlist == None:
            self.data = []
        else:
            self.data = initlist

    def __add__(self, other):

        # For merging two objects, we first need to copy the data,
        # since we want to preserve the original object.
        resultlist = self.data[:]

        # Then, we merge: we have to add only elements not already present
        for element in other.data:
            if element not in resultlist:
                resultlist.append(element)
            
        # Return a new instance of the object containing the results
        return ListUnique(resultlist) 

    def add(self, element):

        # Add an element, if it is not already present
        if element not in self.data:
            self.data.append(element)

    def getData(self):
        return self.data



#######################################################################
#   Class ListAndMerge
#######################################################################
class ListAndMerge(dict):
    """
    To store uniques keys (with one example value) of a dictionary
    """
    
    def __init__(self, initdict=None):

        # This trick prevents linking to the empty dict two differnet objects,
        # and it let to avoid using the in place copy initdic=dic(initdic)
        
        if initdict == None:
            self.data = {}
        else:
            self.data = initdict

    def __add__(self, other):

        # For merging two objects, we first need to copy the data,
        # since we want to preserve the original object.
        returndict = dict(self.data)
        
        # Then, we merge: we have to add only elements not already present 
        for key in other.data:
            if not returndict.has_key(key):
                returndict[key] = other.data[key]

        return ListAndMerge(returndict)


    def add(self, dictionary):

        # Add an element, if it is not already present
        for key in dictionary:
            if not self.data.has_key(key):
                self.data[key] = dictionary[key]    

    def getData(self):
        return self.data



#######################################################################
#   Class Counter
#######################################################################
class Counter(int):
    """
    Count how many times it is called. The argument can be whatever keyword and even empty.
    """

    def __init__(self, init_to=0):
        self.count = init_to

    def __add__(self, other):
        return Counter(self.count + other.count)

    def add(self, element):       
        # Here we increase the counter discarding the element,
        # which can be whatever thing.
        self.count += 1

    def getData(self):
        return self.count
        
        
#######################################################################
#   Class CounterHz
#######################################################################
class CounterHz(int):
    """
    Count how many times it is called. The argoument can be whatever.
    NB : the division by the windows has to be handle later,
    so we just return the count
    """

    def __init__(self, init_to=0):
        self.count = init_to

    def __add__(self, other):        
        return CounterHz(self.count + other.count)

    def add(self, element):       
        # Here we increase the counter discarding the element,
        # which can be whatever thing.
        self.count += 1

    def getData(self):
        return self.count
        
  
 
#######################################################################
#   Class MaxMsgsPerSecOverMinute
#######################################################################
class MaxMsgsPerSecOverMinute(int):
    """
    Over one minute interval, count the messages on one second interval. Then get the maximum value
    """

    first_time = True
    offset = 0

    def __init__(self):
        self.minute = []
        for _ in range(0, 60):
            self.minute.append(0)

    def __add__(self, other):
        # This is meant to be an object used with one bin. Since whe calculating the result
        # to give from this data object we merge a empty resultBin with a realBin with the data,
        # we basically retrn realBin (other) which has data inside it. Yes. it's tricky.. :/
        return other

    def add(self, TIMESTAMP):       

        # We have the TIMESTAMP, like: 2012-01-27T16:06:11+01:00
        
        # Get the second of the timestamp: 
        second = int(TIMESTAMP.split(':')[2][:2])

        if self.first_time:
            self.first_time = False
            self.offset = second
            
        # mod 60 math..
        if second - self.offset < 0:
            second += 60
            
        # Increase the counter for this second..     
        self.minute[second - self.offset] += 1
        

    def getData(self):
        Max = 0
        for i in range(0, 60):
            if self.minute[i] > Max:
                Max = self.minute[i]
                
        return Max 
 
 

#######################################################################
#   Class EstimateThroughputOverMinute
#######################################################################
class EstimateThroughputOverMinute(int):
    """
    Over one minute interval, count the messages on one second interval. Then get the difference between the maximum and the mean values.
    """

    first_time = True
    offset = 0

    def __init__(self):
        self.minute = []
        for _ in range(0, 60):
            self.minute.append(0)

    def __add__(self, other):
        # This is meant to be an object used with one bin. Since whe calculating the result
        # to give from this data object we merge a empty resultBin with a realBin with the data,
        # we basically retrn realBin (other) which has data inside it. Yes. it's tricky.. :/
        return other

    def add(self, TIMESTAMP):       

        # We have the  splitThe
        
        # Get the second of the timestamp: 
        second = int(TIMESTAMP.split(':')[2][:2])

        if self.first_time:
            self.first_time = False
            self.offset = second
            
        # mod 60 math..
        if second - self.offset < 0:
            second += 60
            
        # Increase the counter for this second..     
        self.minute[second - self.offset] += 1
        

    def getData(self):
        Max = 0
        Mean = 0
        for i in range(0, 60):
            Mean += self.minute[i]
            if self.minute[i] > Max:
                Max = self.minute[i]
                
        Mean = Mean / 60
                     
        return Max - Mean
 
 
 
 
#######################################################################
#   Class Adder (inherits from int)                                 #
#######################################################################
class Adder(float):
    """
    Sum up the values passed as argouments.
    """

    def __init__(self, init_to=0):
        self.partialsum = init_to

    def __add__(self, other):
        return Adder(self.partialsum + other.partialsum)

    def add(self, element):       
        self.partialsum += float(element)

    def getData(self):
        return self.partialsum
        
        
#######################################################################
#   Class AdderMB
#######################################################################
class AdderMB(float):
    """
    Sum up the values passed as argouments in bytes and returns in MB.
    """

    def __init__(self, init_to=0):
        self.partialsum = init_to

    def __add__(self, other):
        return AdderMB(self.partialsum + other.partialsum)

    def add(self, element):       
        self.partialsum += float(element)

    def getData(self):
        return self.partialsum / (1024 * 1024)

#######################################################################
#   Class AdderGB
#######################################################################
class AdderGB(float):
    """
    Sum up the values passed as argouments in bytes and returns in GB.
    """

    def __init__(self, init_to=0):
        self.partialsum = init_to

    def __add__(self, other):
        return AdderGB(self.partialsum + other.partialsum)

    def add(self, element):       
        self.partialsum += float(element)

    def getData(self):
        return self.partialsum / (1024 * 1024 * 1024)
        
        
        

#######################################################################
#   Class Max
#######################################################################
class Max(int):
    """
    Keeps the maximum value.
    """

    def __init__(self, init_to=None):
        self.max_value = init_to

    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.max_value == None and other.max_value == None:
            return Max()
        
        if self.max_value == None:
            return Max(other.max_value)
            
        if other.max_value == None:
            return Max(self.max_value)
    
        # Compute the maximum over the two merged bins...    
        if self.max_value > other.max_value:        
            return Max(self.max_value)
            
        else:
            return Max(other.max_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.max_value == None:
            self.max_value = element
        
        # Check for the maximum
        if element > self.max_value:        
            self.max_value = element
            

    def getData(self):
        return self.max_value
        
        
        
#######################################################################
#   Class Max
#######################################################################
class MaxGB(int):
    """
    Keeps the maximum value and returns it in GB provided it is stored in bytes.
    """

    def __init__(self, init_to=None):
        self.max_value = init_to

    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.max_value == None and other.max_value == None:
            return MaxGB()
        
        if self.max_value == None:
            return MaxGB(other.max_value)
            
        if other.max_value == None:
            return MaxGB(self.max_value)
    
        # Compute the maximum over the two merged bins...    
        if self.max_value > other.max_value:        
            return MaxGB(self.max_value)
            
        else:
            return MaxGB(other.max_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.max_value == None:
            self.max_value = element
        
        # Check for the maximum
        if element > self.max_value:        
            self.max_value = element
            

    def getData(self):
        return self.max_value / (1024 * 1024 * 1024)        
        
        
#######################################################################
#   Class Min
#######################################################################
class Min(int):
    """
    Keeps the minimum value.
    """

    def __init__(self, init_to=None):
        self.min_value = init_to

    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.min_value == None and other.min_value == None:
            return Min()
        
        if self.min_value == None:
            return Min(other.min_value)
            
        if other.min_value == None:
            return Min(self.min_value)
            
        # Compute the minimum over the two merged bins...
        if self.min_value < other.min_value:        
            return Min(self.min_value)
            
        else:
            return Min(other.min_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.min_value == None:
            self.min_value = element
        
        # Check for the maximum
        if element < self.min_value:        
            self.min_value = element
            
    def getData(self):
        return self.min_value
        
        
    
#######################################################################
#   Class InverseMin
#######################################################################
class InverseMin(int):
    """
    Keeps the minimum value and return the inverse (1/Min).
    """

    def __init__(self, init_to=None):
        self.min_value = init_to

    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.min_value == None and other.min_value == None:
            return InverseMin()
        
        if self.min_value == None:
            return InverseMin(other.min_value)
            
        if other.min_value == None:
            return InverseMin(self.min_value)
            
        # Compute the minimum over the two merged bins...
        if self.min_value < other.min_value:        
            return InverseMin(self.min_value)
            
        else:
            return InverseMin(other.min_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.min_value == None:
            self.min_value = element
        
        # Check for the maximum
        if element < self.min_value:        
            self.min_value = element
            
    def getData(self):
        return 1 / self.min_value        
        
        
#######################################################################
#   Class LogMin
#######################################################################
class LogMin(int):
    """
    Keeps the minimum value and return the base 10 logarithm
    """

    def __init__(self, init_to=None):
        self.min_value = init_to

    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.min_value == None and other.min_value == None:
            return LogMin()
        
        if self.min_value == None:
            return LogMin(other.min_value)
            
        if other.min_value == None:
            return LogMin(self.min_value)
            
        # Compute the minimum over the two merged bins...
        if self.min_value < other.min_value:        
            return LogMin(self.min_value)
            
        else:
            return LogMin(other.min_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.min_value == None:
            self.min_value = element
        
        # Check for the maximum
        if element < self.min_value:        
            self.min_value = element
            
    def getData(self):
    
        if self.min_value <= 0:
            return 0
        else:    
            return math.log10(self.min_value)


#######################################################################
#   Class LogMax
#######################################################################
class LogMax(int):
    """
    Keeps the maximum value in bytes and returns the logaithm.
    """

    def __init__(self, init_to=None):
        self.max_value = init_to


    def __add__(self, other):
    
        # The following if will handle the case in 
        # which we are merging with an empty bin.        
        if self.max_value == None and other.max_value == None:
            return LogMax()
        
        if self.max_value == None:
            return LogMax(other.max_value)
            
        if other.max_value == None:
            return LogMax(self.max_value)
    
        # Compute the maximum over the two merged bins...    
        if self.max_value > other.max_value:        
            return LogMax(self.max_value)
            
        else:
            return LogMax(other.max_value)


    def add(self, element):
    
        # Make sure that we are adding a numeric type    
        if type(element) not in ['int', 'float']:
            # If we are trying to add a non-numeric value, try to convert it to float
            element = float(element)
      
        # Here we are at the first addedd value
        if self.max_value == None:
            self.max_value = element
        
        # Check for the maximum
        if element > self.max_value:        
            self.max_value = element
            

    def getData(self):
        return math.log10(self.max_value)

#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import calendar

    
    
#---------------------------------------------------------------------------
def timestamp_to_epoch(msg_timestamp, msg_epoch=None):
    """
    Timestamp (i.e. 2011-08-23T10:27:35+02:00) to epoch seconds conversion
    """
    
    # First, some hypothesis and concepts:
    
    # UTC (Unified Temps Coordinated) = GMT (Greenwich Mean Time)
    
    # Leap seconds:
    # Added to the year duration to compensate wrong year estimation, epoch time will just be oen second longer:
    # 798 -> 799 -> 800 -> 800 -> 801 -> 802
    # biut be careful: 800.25 800.50 800.75 800.0 800.25 etc.. -> risk of negative numbers in calculating deltas
    
    # Summertime:
    # UTC+1 becomes UTC+2, and the local hour changes: it is like if parallels are shifted, 
    # so handling timezones will automatically handle summertime.
    
    # EPOCH (Unix time) definition:
    # it is the number of seconds elapsed since midnight UTC on the morning 
    # of January 1, 1970, not counting leap seconds
    
    if (msg_epoch == None):
        # There is no epoch filed that we can use, we have to calculate the epoch from the timestamp...
        # ..so, we have a timestamp in the form: 2011-08-23T10:27:35+02:00. 
        # let's split it, since Python support for epoch with timezone sucks
        msg_timezone_sign = msg_timestamp[-6] 
        msg_timezone = msg_timestamp[-5:]
        msg_timestamp = msg_timestamp[:-6]

        # Convert the timezone into seconds, preserve the sign
        msg_timezone_hour, msg_timezone_minutes = msg_timezone.split(':')
        msg_timezone_epoch = int(msg_timezone_sign + '1') * ((int(msg_timezone_hour) * 3600) + (int(msg_timezone_minutes) * 60))

        # compute the epoch
        epoch = calendar.timegm((int(msg_timestamp[0:4]),  # tm_year
                                 int(msg_timestamp[5:7]),  # tm_mon
                                 int(msg_timestamp[8:10]),  # tm_mday
                                 int(msg_timestamp[11:13]),  # tm_hour
                                 int(msg_timestamp[14:16]),  # tm_min
                                 int(msg_timestamp[17:19]),  # tm_sec
                                 - 1,  # tm_wday
                                 - 1,  # tm_yday
                                 - 1)) - msg_timezone_epoch  # tm_isdst
        return epoch
        
    else:
     
        # There is a field with the epoch alreay computed in the message, we can use it!
        # EPOCH field it has been calculated by Scribe injectors on every node.
        # This trick pulls down execution time on the test data set from 14 to 10 seconds!!
        
        return int(msg_epoch)
        
        
        
        
# # Pretty printing...
#---------------------------------------------------------------------------
def recursive_print(_bin, depth, groupbykeys, datakeys):
    for subdic in _bin, depth, groupbykeys, datakeys:           
        print ""
        for _ in range(0, depth):
            print "\t",

        print str(subdic) + ":"
        if not depth == len(groupbykeys) - 1:
            

            # Again          
            recursive_print(bin[subdic], depth + 1, groupbykeys, datakeys) 
        else:

            pos = 0
            # Here we are at the datalevel
            for dataobject in bin[subdic]:
                # print type(dataobject)
                # print dataobject

                
                # print a nice list:
                if type(dataobject.getData()) is list:
                    print ""
                    # Align it
                    for _ in range(0, depth + 1):
                        print "\t",
                    print str(type(dataobject)).split('.')[1].split("'")[0] + "(" + datakeys[pos] + "):"  # .split('.')[1].split("'")[0]

                    for item in dataobject.getData():                        
                        # Align it
                        for _ in range(0, depth + 1):
                            print "\t  ",
                        
                        print item

                else:
                    print ""
                    # Align it
                    for _ in range(0, depth + 1):
                        print "\t",
                    print str(type(dataobject)).split('.')[1].split("'")[0] + "(" + datakeys[pos] + "):"

                    # Align it
                    for _ in range(0, depth + 1):
                        print "\t  ",
                    print str(dataobject.getData())
                pos += 1

            print ""

# # Pretty printing standard...
#---------------------------------------------------------------------------
def recursive_print_std(_bin, depth, groupbykeys):
    for subdic in _bin:           
        print ""
        for _ in range(0, depth):
            print "\t",

        print str(subdic) + ":"
        if not depth == len(groupbykeys) - 1:
            

            # Again          
            recursive_print_std(bin[subdic], depth + 1, groupbykeys) 
        else:

            pos = 0
            # Here we are at the datalevel
            for dataobject in bin[subdic]:
                # Align it
                for _ in range(0, depth + 1):
                    print "\t",
                # for more verobose printing, unvomment the following
                # print self.dataobjects[pos]+"("+self.datakeys[pos]+"):",
                print str(dataobject.getData())
                pos += 1

            print ""


# This is a fuction useful for debugging..
#---------------------------------------------------------------------------
def printTree(tree, depth=0):
    if tree == None or len(tree) == 0:
        print "\t" * depth, "-"
    else:
        for key, val in tree.items():
            print "\t" * depth, key
            printTree(val, depth + 1)


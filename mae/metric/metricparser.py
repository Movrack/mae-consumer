#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import re 
import logging
from metricexceptions import InvalidMetricException


regexKeyValue = re.compile('(\w*) *: *(.*)')

class MetricParser(object):

    def parseMetrics(self, metricsfile):
        logging.debug("Loading metrics from " + metricsfile)
        
        # Load the metric from file
        with open(metricsfile) as f:
            lines = f.readlines()
        
    
        # Initilazion...
        metrics=[]
        metric={}
    
        metric_is_opened = False
        opened_metric = 0
        closed_metric = 0
        
        # extract key values:
        for line in lines:
            
            # Remove space and empty lines
            line = line.strip()
            if 0 == len(line) :
                continue
            
            # If there is the comment symbol, skip
            if line[0] == '#' or line[0] == ';':
                continue
                
            # Check beginning of the metric
            if '<metric>' in line:
                metric_is_opened = True
                opened_metric += 1
                continue
                
                
            # Check if the metric we are analyzing is closed
            if '</metric>' in line:
                metric_is_opened = False
                closed_metric += 1
                
                # The metric is ended, lety's check that we have all the needed keywords:
                if not (metric.has_key('name') and metric.has_key('window') and metric.has_key('conditions') \
                        and metric.has_key('conditions') and metric.has_key('groupbykeys') and metric.has_key('data') \
                        and metric.has_key('handle_unordered') and metric.has_key('nbins')):
                    raise InvalidMetricException("Metric at "+metricsfile + "missing some keywords.")
    
                logging.info('Found valid metric : "' + metric['name'] + '"')
    
                # support old metric without unit field
                if not metric.has_key('unit'):
                    metric['unit'] = ''
    
                # support old metric without category field
                if not metric.has_key('category'):
                    metric['category'] = ''
    
                    
                # Perfect, we have all the keys. Let's now rearrange the data array:
                metric['groupbykeys'] = metric['groupbykeys'].replace(' ', '').split(',')
                
                # remove space and split on ","
                datas = metric['data'].replace(' ', '').split(',') 
                metric['data'] = []
               
                for datadefinition in datas:
                    data_key = datadefinition.split('(')[1][:-1]
                    data_object =  datadefinition.split('(')[0]
        
                    metric['data'].append( (data_key, data_object) )
                    
                    # If yes definition exist : add it to metric
                    metrics.append(metric)
        
                # Clear dictionary (will this work?!
                metric={}
    
    
    
            # Extract key: value
            if metric_is_opened:  
                regexResult = regexKeyValue.search(line)
                keyword = regexResult.group(1)
                value = regexResult.group(2)
                metric[keyword] = value
        
            
        if closed_metric != opened_metric :
            raise InvalidMetricException("Invalide metric file. \"<metric>\" or \"</metric>\" missing in "+metricsfile)
    
        return metrics




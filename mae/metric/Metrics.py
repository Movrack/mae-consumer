#!/usr/bin/python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

import logging
import metricutils
import glob
import Queue
from metricparser import MetricParser
from MetricsAnalysisEngine import Bins
from metricexceptions import InvalidMetricException

#######################################################################
#   Class Metric                                                      #
#######################################################################

class Metric(object):
    
    def __repr__(self):
        return "<\'" + self.name + "\' metric object>"
    
    #---------------------------------------------------------------------------
    def __init__(self, name, unit, category, window, nbins, conditions, groupbykeys, data, handle_unordered):

        # Local vars..

        # - name, string
        self.name = name

        # - unit, string
        self.unit = unit

        # - unit, string
        self.category = category
        
        # - window, int
        self.window = int(window)
        
        # - nbins, int
        self.nbins = int(nbins)

        # - conditions, string    
        self.conditions = conditions

        # - handle_unordered, string
        self.handle_unordered = handle_unordered

        # - groupbykeys, list
        self.groupbykeys = groupbykeys

        # - data, dictionary

        # Convert from list of touples to two lists
        # (should be changed in future, to use the dictionary everywhere instead of two separate lists).
        self.datakeys = []
        self.dataobjects = []
        
        for touple in data:
            self.datakeys.append(touple[0])
            self.dataobjects.append(touple[1])
        
               
        # Define the "safe" functions. Only needed to be able to use complex statements
        # in the conditions field. Uncomment and see the function checkconditions() to enable it.
        # self.safefunctions={'True':True,'False':False}   
            
        # Compile the test that decide if the message being evaluated belongs to the metric:
        try:                
            if conditions != 'NONE':
            
                # Compiling the test makes a speedup of about 10%
                self.test = compile(conditions, '', 'eval')

                # For benchmarking without compiling the test:
                # self.test=conditions
     
        except:
        
            # If we were not able to compile the test for whatever reason, we cannot go ahead.
            # Print the exception and exit.
            # print self.name+": Unexpected error while compiling the metric (Err except: "+str(sys.exc_info()[0])+")"
            # sys.exit (1)

            # reraise exception, to handle it later
            raise 

        # Just a counter...
        self.accepted = 0
        self.reporterQueue = Queue.Queue()
        # Create the bins
        self.metricBins = Bins(self.name, self.window, self.nbins, self.groupbykeys, self.dataobjects, self.handle_unordered, self.reporterQueue)
        
        # Debug...
        # print self.metricBins.bins



    #---------------------------------------------------------------------------
    def addData(self, msg):

        # Fill the groupbyvalues and datavalues lists:

        groupbyvalues = []
        for key in self.groupbykeys:
            groupbyvalues.append(msg[key])


        # --KEYPOINT--        
        # Prepare the datavalues
        
        # There are some special keywords that will affect the data preparation,
        # this is important to keep in mind in case of further editing.

        datavalues = []
        
        for key in self.datakeys:

            # This handles objects that have to operate on the entire message
            # (i.e.: ListAndMerge(KEYVALUES) or DummyStore(KEYVALUES))
            if key == 'KEYVALUES':
                datavalues.append(msg)
        
            # This handles an object without arguments (i.e.: Counter() )
            elif key == '':
                datavalues.append('')
        
            # In all the other cases, take the right value for the keyword
            else:
                datavalues.append(msg[key])


        # Add the data to the bins        
        if msg.has_key('EPOCH'):
            self.metricBins.addData(groupbyvalues, datavalues, msg['TIMESTAMP'], msg['EPOCH'])
        else:
            self.metricBins.addData(groupbyvalues, datavalues, msg['TIMESTAMP'])
        

        # If you do not have a try-except handler at an higher level, i.e. you are using
        # this module standalone, you may want to do something like:
        
        # try:
        #     self.metricBins.addData(groupbyvalues,datavalues,msg['TIMESTAMP'])
        # except:
        #     logging.debug( "ERROR, caused by:" )
        #     logging.debug( msg )
                   


    #---------------------------------------------------------------------------
    def getData(self, method='returnDict', timeout=False):

        # How to get data out from the metric. The standard method is returnDict, which gives back
        # a python dictionary, in which bins are already merged and final data (like averages) is already computed

        if method == 'returnDict':
            return self.metricBins.getData('All')
            
        if method == 'AllShiftedByOne':
            return self.metricBins.getData('AllShiftedByOne')

        if method == 'Queue':
            try:
                logging.debug("get in queue of size = "+ str(self.reporterQueue.qsize()))
                return self.reporterQueue.get(False, timeout)
            except Queue.Empty:
                return None
                
        # The next functions are here for debugging / proof of concepts.
        # The idea is that the metric should give just the dictionary
        # cointaining the aggregated data already computed, and then
        # should be up to the next layer how to deal with it.
        if method == 'print':
            print ""
            print "====== " + self.name + " ======"
            metricutils.recursive_print(self.metricBins.getData(), 0, self.groupbykeys, self.datakeys)

        if method == 'print_std':
            print ""
            print "====== " + self.name + " ======"
            metricutils.recursive_print_std(self.metricBins.getData(), 0, self.groupbykeys)



    #---------------------------------------------------------------------------
    def checkconditions(self, msg):

        # 1) Check that the current message has every keyword needed for the metric:
    
        # a) on the groupby
        for keyword in self.groupbykeys:
            if not msg.has_key(keyword):
                return False

        # b) on the data (if not empty)
        for keyword in self.datakeys:
            
            # Skip the test if we have a special keyword:
            if (keyword == 'KEYVALUES' or keyword == ''):
                pass
            
            # Switch to the next one, please...    
            # if keyword not in ['KEYVALUES','']:        
                
            else:
                if not msg.has_key(keyword):
                    return False

        # 2) Check that the conditions match our metric 
        try:
   
            if self.conditions != 'NONE':
            
                # To be able to use complex statements in the conditions field, use
                # the following. but it costs +10% execution time.
                
                # safedomain = dict(msg.items() + self.safefunctions.items())
                safedomain = msg

                if eval(self.test, {"__builtins__":None}, safedomain):                                     
                    # Message accepted from metric
                    
                    # If you want hard verbosity debugging uncomment the following...        
                    # print self.name+": METRIC MATCHED"    
                    return True
                    
                else:   
                    # Message rejected from metric
                    return False
                    
            else:
                return True
                
        except:
        
            # If there is an exception here, most likely it is because one of the keywords were
            # not found in the message, so the message is not even a candidate for this metric

            return False
        
        # If you want hard verbosity debugging uncomment the following...        
        # print self.name+": METRIC MATCHED"    



    #---------------------------------------------------------------------------
    def apply(self, msg):     
        if msg.has_key('MSG_NOT_VALID'):
            return
        # Check that there is the TIMESTAMP keyword, which is mandatory:        
        if msg.has_key('TIMESTAMP'):
            # Ok, now add some custom keywords:

            # For Count and Delay special object
            msg['COUNT'] = '+1'  # Deprecated...
            msg['DELAY'] = '0'
                     
            # For grouping by nothing:
            msg['NONE'] = 'Grouped by NONE'
            
            # Easier date for grouping by           
            msg['DATE'] = msg['TIMESTAMP'].split('T')[0]
        
        
            # Evaluate the message: does it match the metric?
            if self.checkconditions(msg):
                logging.debug("cond : ok : add data")
                # Add this data to the metric
                self.addData(msg)
                
                # Increrase the counter of accepted messages
                self.accepted += 1
            else: 
                logging.debug("cond : ko")



# Parse a file and load metrics data...
#---------------------------------------------------------------------------
def loadMetrics(path):

    # Prepare the array..
    metrics = []
    
    metricParser = MetricParser()

    for metricfile in glob.glob(path):

        # Load the metric from file
        try : 
            parsed_metrics = metricParser.parseMetrics(metricfile)
        except InvalidMetricException, e: 
            logging.info(e.msg)

        # For every metric read..
        for metric in parsed_metrics:

            # Create the metric
            # def __init__( self, name, unit, category, window, nbins, conditions, groupbykeys, data, handle_unordered):
            try:
                metrics.append(Metric(metric['name'],
                                      metric['unit'],
                                      metric['category'],
                                      metric['window'],
                                      metric['nbins'],
                                      metric['conditions'],
                                      metric['groupbykeys'],
                                      metric['data'],
                                      metric['handle_unordered']))
            except SyntaxError:
                logging.warning("Syntax Error in metric " + metric['name'])
    return metrics


#!/usr/bin/env python
# -*- coding: utf-8 -*-

#******************************************************************************
#                     mae-consumer
#
# Copyright (C) 2013  CERN
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# @author: Castor dev team
#******************************************************************************

""" Retrive log and make sort """

import sys
import json
import getopt
import traceback


class Mapper:
    def __init__(self, dateStart, dateEnd, instanceName="", filterValue=""):
        self.instanceName = instanceName
        self.filter = filterValue
        self.dateStart = dateStart
        self.dateEnd = dateEnd

        try:
            if self.filter != "":
                self.test = compile(self.filter, '', 'eval')
            else:
                self.test = self.filter
        except Exception, e:
            #rint " >>>> Error : "+str(e)
            #raceback.print_exc()
            #rint " <<<"
            pass
        #print "filter > "+self.filter

    def is_good_log(self, dictionnaryLog):
        if "TIMESTAMP" in dictionnaryLog and not self.dateStart[0:10] <= dictionnaryLog['TIMESTAMP'][0:10] <= self.dateEnd[0:10]:
            return False

        try:
            if (self.instanceName != "") and (dictionnaryLog['INSTANCE'] != self.instanceName):
                return False

        except KeyError:
            # Key not present
            return False

        try:
            if not eval(self.test, {"__builtins__": None}, dictionnaryLog):
                return False
        except Exception, e:
            #rint " >>>> Error : "+str(e)
            #traceback.print_exc()
            #rint " <<<"
            pass

        return True

    def execute(self):

        for line in sys.stdin:
            line = line.strip()
            # empty string ?
            if line.isspace() or line == "":
                continue
            try:
                dicLog = json.loads(line)

                if not self.is_good_log(dicLog):
                    # Go to next log
                    continue

            except Exception, e:
               #print " >>>> Error : "+str(e)
               #traceback.print_exc()
               #print " <<<"
               #sys.exit(2)
               pass


            # Use log for reduce
            print line


class Main:
    def __init__(self, argv):
        self.argv = argv
        self.instanceName = ""
        self.filterValue = ""
        self.dateStart = None
        self.dateEnd = None

    def parse_argv(self):
        helpPrint = "usage : " + self.argv[0] + ' [-i <instance>] [-f <filter>] DateStart DateEnd '

        try:
            opts, args = getopt.getopt(self.argv, "hi:f:", ["instance=", "filter="])
        except getopt.GetoptError:
            print helpPrint
            sys.exit(2)

        # Check flag parameter
        for opt, arg in opts:
            if opt == '-h':
                print helpPrint
                sys.exit()
            elif opt in ("-i", "--instance"):
                self.instanceName = arg
            elif opt in ("-f", "--filter"):
                self.filterValue = arg

        if len(args) < 2:
            print "There is no dates."
            print helpPrint
            sys.exit(2)

        #print self.argv
        self.dateStart = self.argv[0]
        self.dateEnd = self.argv[1]

    def run_mapper(self):
        mapper = Mapper(self.dateStart, self.dateEnd, self.instanceName, self.filterValue)
        mapper.execute()


def main(argv):
    main = Main(argv)
    main.parse_argv()
    main.run_mapper()

if __name__ == "__main__":
    main(sys.argv[1:])

%define name mae-consumer
%define version 1.1.2
%define release 1

%{expand:%define PYTHON_LIBDIR %(python -c "from distutils import sysconfig; print sysconfig.get_python_lib()")}

Summary: Mae Consumer - An multi process Analyser Engine to compute metrics on logs file.
Name: %{name}
Version: %{version}
Release: %{release}%{?dist}
Source: %{name}-%{version}.tar.gz
License: GPL
Group: Development/Libraries
BuildRoot: %{_builddir}/%{name}-%{version}-root
BuildArch: noarch
Requires: python >= 2.6, python-simplejson python-dirq python-messaging python-rpyc hadoop-client >= 2.0.0, hadoop-mapreduce >= 2.0.0,
Url: http://castor.web.cern.ch

%description
castor (Cern Advanced STORage system) - AI messaging consumer for the Metric Analysis Engine.


Usage is "python maemanager.py path/to/config_file

Default logging path is     /var/log/maemanager.log
it can be modified in config file.

Input plugins path is       mae/analyzerinputplugin
Output plugins path is      mae/reduceroutputplugin

NB : python >= 2.6, simplejson, python-dirq and python-messaging, python-rpyc, hadoop-mapreduce >= 2.0.0, hadoop-client >= 2.0.0 are required
They can be found here : https://twiki.cern.ch/twiki/bin/view/EMI/MessagingLibraries

%prep
%setup -q

%install
install -d $RPM_BUILD_ROOT/usr/bin -m 0755
install -d $RPM_BUILD_ROOT/var/log -m 0755
install -d $RPM_BUILD_ROOT/etc/mae/analyzerinputplugin -m 0755
install -d $RPM_BUILD_ROOT/etc/mae/reduceroutputplugin -m 0755
install -d $RPM_BUILD_ROOT/etc/mae/hadoop -m 0755
install -d $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae -m 0755
install -d $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop -m 0755
install -d $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric -m 0755

#basics file
install -m 755 maemanager.py $RPM_BUILD_ROOT/usr/bin
install -m 644 maemanager.conf.EXAMPLE $RPM_BUILD_ROOT/etc/mae

#files input plugins
install -m 644 analyzerinputplugin/plugin_example.py $RPM_BUILD_ROOT/etc/mae/analyzerinputplugin
install -m 644 analyzerinputplugin/dirq_reader.py $RPM_BUILD_ROOT/etc/mae/analyzerinputplugin
install -m 644 analyzerinputplugin/file_reader.py $RPM_BUILD_ROOT/etc/mae/analyzerinputplugin

#files output plugins
install -m 644 reduceroutputplugin/plugin_example.py $RPM_BUILD_ROOT/etc/mae/reduceroutputplugin
install -m 644 reduceroutputplugin/cockpit_pusher.py $RPM_BUILD_ROOT/etc/mae/reduceroutputplugin

#files /mae
install -m 644 mae/__init__.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/analyzer.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/consumer.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/reducer.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/reporter.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/rpycserverdeamon.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/utils.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae
install -m 644 mae/threadpoolserver.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae

#files /mae/hadoop
install -m 644 mae/hadoop/__init__.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop
install -m 644 mae/hadoop/HadoopRequester.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop
install -m 644 mae/hadoop/hadooprequestexception.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop
install -m 644 mae/hadoop/mapperFindAndSort.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop
install -m 644 mae/hadoop/mapperFindAndSort.py $RPM_BUILD_ROOT/etc/mae/hadoop
install -m 644 mae/hadoop/hadoop-streaming-2.0.0-mr1-cdh4.1.2.jar $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/hadoop
install -m 644 mae/hadoop/hadoop-streaming-2.0.0-mr1-cdh4.1.2.jar $RPM_BUILD_ROOT/etc/mae/hadoop

#files /mae/metric
install -m 644 mae/metric/__init__.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/analyzerfunc.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/metricexceptions.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/metricparser.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/Metrics.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/MetricsAnalysisEngine.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric
install -m 644 mae/metric/metricutils.py $RPM_BUILD_ROOT/%{PYTHON_LIBDIR}/mae/metric

%clean
%{__rm} -rf $RPM_BUILD_ROOT
%{__rm} -rf $RPM_BUILD_DIR/%{name}-%{version}

%files 
%defattr(-,root,root)
%attr(-,root,bin) %dir /var/log
%attr(0644,root,root) %doc /etc/mae/maemanager.conf.EXAMPLE 

%attr(0755,root,root) /usr/bin/maemanager.py*

%attr(0644,root,root) /etc/mae/analyzerinputplugin/plugin_example.py*
%attr(0644,root,root) /etc/mae/analyzerinputplugin/dirq_reader.py*
%attr(0644,root,root) /etc/mae/analyzerinputplugin/file_reader.py*

%attr(0644,root,root) /etc/mae/reduceroutputplugin/plugin_example.py*
%attr(0644,root,root) /etc/mae/reduceroutputplugin/cockpit_pusher.py*

%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/__init__.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/analyzer.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/consumer.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/reducer.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/reporter.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/rpycserverdeamon.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/utils.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/threadpoolserver.py*

%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/hadoop/__init__.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/hadoop/HadoopRequester.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/hadoop/hadooprequestexception.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/hadoop/mapperFindAndSort.py*
%attr(0644,root,root) /etc/mae/hadoop/mapperFindAndSort.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/hadoop/hadoop-streaming-2.0.0-mr1-cdh4.1.2.jar*
%attr(0644,root,root) /etc/mae/hadoop/hadoop-streaming-2.0.0-mr1-cdh4.1.2.jar*


%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/__init__.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/analyzerfunc.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/metricexceptions.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/metricparser.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/Metrics.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/MetricsAnalysisEngine.py*
%attr(0644,root,root) %{PYTHON_LIBDIR}/mae/metric/metricutils.py*

#%post
#/sbin/chkconfig --add maemanager
#/sbin/service maemanager condrestart > /dev/null 2>&1 || :

#%preun
#if [ "$1" = 0 ]; then
#    /sbin/service maemanager stop > /dev/null 2>&1 || :
#    /sbin/chkconfig --del maemanager
#fi


%changelog
* Thu Aug 29 2013  Manuel Servais <manuel.servais@cern.ch> 1.1.1-1
- Some enhancement and fixing.

* Thu Aug 13 2013  Manuel Servais <manuel.servais@cern.ch> 1.1.0-3
- Change requires in spec file

* Thu Aug 13 2013  Manuel Servais <manuel.servais@cern.ch> 1.1.0-2
- Add requires in spec file, readme and GPL licence in files

* Thu Aug 13 2013  Manuel Servais <manuel.servais@cern.ch> 1.1.0
- Initial packaging on the new parallelized version

* Thu May 29 2012 Benjamin Fiorini <benjamin.fiorini@cern.ch> 1.0.0
- First implementation.

